# @universis/viber-connect

[Universis](https://gitlab.com/universis/universis-api) instant messaging plugin for Viber 

## Installation

npm i @universis/viber-connect

## Usage

Register `ViberConnectService` in application services

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/viber-connect#ViberConnectService"
        }
    ]

Add `ViberConnectSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/viber-connect#ViberConnectSchemaLoader"
                    }
                ]
            }
        }
    }

Add viber-connect configuration:

# app.production.json

{
    "settings": {
        "universis": {
            "viber-connect": {
                "apifon": {
                    "tokenURL": "https://ids.apifon.com/oauth2/token",
                    "hostURL": "https://ars.apifon.com",
                    "client_id": "<Apifon_Client_ID>",
                    "client_secret": "<Apifon_Client_Secret>",
                    "grant_type": "client_credentials"
                },
                "auth": {
                    "authorizationURL": "https://users.universis.io/authorize",
                    "logoutURL": "https://users.universis.io/logout?continue=http://localhost:5001/services/viber/logout",
                    "tokenURL": "https://users.universis.io/access_token",
                    "scope": "profile",
                    "clientID": "<Your_Universis_Client_ID>",
                    "clientSecret": "<Your_Universis_Client_Secret>",
                    "callbackURL": "http://localhost:5001/services/viber/callback"
                }
            }
        }
    }
}