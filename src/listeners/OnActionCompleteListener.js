/* eslint-disable no-unused-vars */
import { DataError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { ExpressDataContext } from '@themost/express';
import { getMailer } from '@themost/mailer';
import { ViberConnectService } from '../ViberConnectService';
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
	//
}

async function afterSaveAsync(event) {
	// get context
	// eslint-disable-next-line no-unused-vars
	/**
	 * @type {ExpressDataContext}
	 */
	const context = event.model.context;
	/**
	 * @type {ViberConnectService}
	 */
	const service = context.getApplication().getService(ViberConnectService);
	// get current status
	const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
	// get previous status
	let previousStatus = 'UnknownActionStatus';
	if (event.previous) {
		previousStatus = event.previous.actionStatus.alternateName;
	}
	if (actionStatus === 'ActiveActionStatus' && event.state === DataObjectState.Insert) {
		// place your code here
		// send pin to viber
		const person = await context.model('Student')
			.asQueryable()
			.expand({
				name: 'user',
			}, {
				name: 'person',
			})
			.where('user/name').equal(context.user.name)
			.select('person')
			.silent()
			.value();

		const item = await context.model(event.model.name).where('id').equal(event.target.id).expand('owner').getItem();

		// Send a "This is your OTP code" message to the user
		// Replace this code with APIFON api

		service.sendOtp(context, person, item);
	}

	if (actionStatus === 'CompletedActionStatus' && previousStatus !== 'CompletedActionStatus') {
		// place your code here
		const person = await context.model('Student')
			.asQueryable()
			.expand({
				name: 'user',
			}, {
				name: 'person',
			})
			.where('user/name').equal(context.user.name)
			.select('person')
			.silent()
			.value();

		if (person.mobilePhone == null && person.temporaryPhone == null) {
			throw new DataError('BadRequest', 'Your mobile phone number is not defined.');
		}

		// create ViberAccount and save it to user
		const user = await context.model('Users').where('name').equal(context.user.name).expand('viberAccount').silent().getItem();
		// if the user does not have a viber account, then create one and save it into the user
		if (user.viberAccount == null) {
			//save viber account
			await context.model('User').silent().save({
				name: context.user.name,
				viberAccount: {
					identifier: person.mobilePhone ?? person.temporaryPhone,
					name: context.user.name,
				}
			});
		}
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	return beforeSaveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	return afterSaveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}